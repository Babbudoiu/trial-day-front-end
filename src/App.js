import React, {useState, useEffect} from "react";
import './App.css';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {authUser} from "./utils";
import Home from "./components/Home";
import UserLogin from "./components/UserLogin";
import Register from "./components/Register";
import Header from "./components/Header";
import Posts from "./components/Posts";

function App() {
  const [user, setUser] = useState();

  useEffect(() => {
  authUser(setUser)
}, [user])

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/signin">
            <UserLogin setUser={setUser} />
            {!user ? (
              <Redirect to = "/signin" />
            ) : (
                <Redirect to = "/posts" />
            )
          }
          </Route>
          <Route exact path="/register">
            <Register setUser={setUser}/>
          </Route>
          <Route exact path="/posts">
            <Header setUser={setUser} />
            <Posts />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
