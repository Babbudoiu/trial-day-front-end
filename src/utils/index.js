export const createUsers = async (e, username, email, password, setUser, callback) => {
    e.preventDefault();
    try {
        const response = await fetch(`${process.env.REACT_APP_BACK_END}users`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                name: username,
                email: email,
                password: password
            }),
        });
        const data = await response.json();
        setUser(data.user.name);
        callback();
    } catch (error) {
        console.log(error);
    }
};

export const fetchUsers = async (e, username, password, setUser) => {
    e.preventDefault();
    try {
        const response = await fetch(`${process.env.REACT_APP_BACK_END}users/${username}`, {
            method: "POST",
            headers: { "Content-Type": "application.json" },
            body: JSON.stringify({
                password: password
            }),
        });
        const data = await response.json();
        localStorage.setItem("Mytoken", data.token);
        setUser(data.user.name);
    } catch (error) {
        console.log(error);
    }
};

export const authUser = async (setUser) => {
    if (localStorage.MyToken) {
        try {
            const response = await fetch(`${process.env.REACT_APP_BACK_END}users`, {
                method: "GET",
                headers: { Authorization: `Bearer ${localStorage.getItem("MyToken")} ` }
            });
            const data = await response.json();
            setUser(data.name)
        } catch (error) {
            console.log(error);
        }
    }
};

export const createPost = async (e, title, text, tag, setPost) => {
    e.preventDefault();
    try {
        const response = await fetch(`${process.env.REACT_APP_BACK_END}posts`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                title: title,
                text: text,
                tag: tag
            }),
        });
        const data = await response.json();
        setPost(data.post.title);
    } catch (error) {
        console.log(error);
    }
};

export const fetchPosts = async (e, title, setPost) => {
    e.preventDefault();
    try {
        const response = await fetch(`${process.env.REACT_APP_BACK_END}posts/${title}`, {
            method: "GET"
        });
        const data = await response.json();
        setPost(data.post.title);
    } catch (error) {
        console.log(error);
    }
};

export const deletePost = async (e, title, setPost) => {
    e.preventDefault();
    try {
        const response = await fetch(`${process.env.REACT_APP_BACK_END}posts/${title}`, {
            method: "DELETE"
        });
        const data = await response.json();
        setPost();
    } catch (error) {
        console.log(error);
    }
};


     