import React from 'react';
import { NavLink } from 'react-router-dom';
import "../styles/Home.css";

function Home() {
    return (
        <div className="home-container">
            <h1>Today I Have</h1>
            <NavLink to ="/signin">Sign In</NavLink> or {""}
            <NavLink to ="/register">Register</NavLink>
        </div>
    )
}

export default Home;
