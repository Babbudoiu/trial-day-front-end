import React from 'react';
import { useHistory } from 'react-router';

function Header({ setUser }) {
    const history = useHistory();

    const logOutHandler = (e) => {
        e.preventDefault();
        localStorage.removeItem("MyToken");
        setUser();
        history.push("/")
    }
    return (
        <div>
            <button className="nav-btn" onClick={logOutHandler}>Sign Out</button>
        </div>
    )
}

export default Header
