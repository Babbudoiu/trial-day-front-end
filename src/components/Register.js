import React, { useState } from "react";
import { createUsers } from "../utils";
import { useHistory } from "react-router";
import "../styles/Register.css";

function Register({setUser}) {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();
    const history = useHistory();
    const callback = () => {
        history.push("/posts")
    }
    return (
        <div className="form-container"> 
            <form className="log-form"
                onSubmit={(e) => createUsers(e, username, email, password, setUser, callback)}>
                
                <input
                    className="log-input"
                    onChange={(e) => setUsername(e.target.value)}
                    placeholder="Username" />
                <input
                    className="log-input"
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Email" />
                <input
                    className="log-input"
                    onChange={(e) => setPassword(e.target.value)}
                    type="password"
                    placeholder="Password" />
                <button className="register-btn" type="submit">Register</button>
            </form>
        </div>
    )
}

export default Register;
