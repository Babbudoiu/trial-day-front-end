import React, { useState } from 'react';
import { fetchUsers } from "../utils";
import "../styles/UserLogin.css";

function UserLogin({ setUser }) {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    return (
        <div className="login-container">
            <form className="log-form"
                onSubmit={(e) => fetchUsers(e, username, password, setUser)}>
                <input 
                    className="log-input"
                    onChange={(e) => setUsername(e.target.value)}
                    placeholder="Username" />
                <input
                    className="log-input"
                    onChange={(e) => setPassword(e.target.value)}
                    type="password"
                    placeholder="Password" />
                <button className="login-btn" type="submit">Log In</button>
            </form>
        </div>
    )
}

export default UserLogin;
